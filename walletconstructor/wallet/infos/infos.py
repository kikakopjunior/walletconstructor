from web3 import Web3
from walletconstructor.security.security import Security
from decimal import Decimal, ROUND_DOWN
import requests

from walletconstructor.wallet.tokens import Tokens
from walletconstructor.wallet.infos.config import ROUNDING_PRECISION, PRICE_ETH

class Infos(dict):
    ether_price = PRICE_ETH
    
    def __init__(self, web3: Web3, security: Security, tokens: Tokens, *args, **kwargs) -> None:
        self._web3 = web3
        self._security = security
        self._tokens = tokens
        super().__init__(*args, **kwargs)
        self._initialisation_eth()
        self._initialisation_tokens()
        self._initialisation_total()
        self.update_infos()

    def _initialisation_eth(self) -> None:
        self['eth'] = {}
        self['eth']['sold'] = {}
        self['eth']['balance'] = Decimal('0')
        return None

    def _initialisation_tokens(self) -> None:
        self['tokens'] = {}
        for k in self._tokens.keys():
            self['tokens'][k] = {}
            self['tokens'][k]['sold'] = {}
            self['tokens'][k]['balance'] = Decimal('0')
        return None

    def _initialisation_total(self) -> None:
        self['total'] = {}
        for k in PRICE_ETH.keys():
            self['total'][k] = Decimal('0')
        return None

    def __balance_eth(self) -> None:
        try:
            balance_wei_eth = self._web3.eth.get_balance(self._security.addr_ethereum)
            balance_eth = Decimal(self._web3.from_wei(balance_wei_eth, "ether")).quantize(ROUNDING_PRECISION, rounding=ROUND_DOWN)
            self['eth']['balance'] = balance_eth
        except Exception as e:
            raise Exception("Erreur lors de la récupération du solde en ether") from e
    
    def __sold_eth(self) -> None:
        try:
            ether_balance = self['eth']['balance']
            ether_price = Infos.ether_price

            for k, v in ether_price.items():
                decimal_value = Decimal(v)
                self['eth']['sold'][k] = (ether_balance * decimal_value).quantize(ROUNDING_PRECISION, rounding=ROUND_DOWN)
        except Exception as e:
            raise Exception("Erreur lors du calcul du solde en ether") from e

    def __init_eth_infos(self) -> None:
        self.__balance_eth()
        self.__sold_eth()

    def update_eth(self) -> None:
        self.__init_eth_infos()

    def update_tokens(self) -> None:
        self._initialisation_tokens()
        self.__balance_tokens()
        self.__sold_tokens()

    def update_infos(self) -> None:
        self.update_eth()
        self.update_tokens()
        self.__sold_total()

    def __balance_tokens(self) -> None:
        for k, v in self._tokens.items():
            balance = v.balance(self._security)
            self['tokens'][k]['balance'] = Decimal(balance).quantize(ROUNDING_PRECISION, rounding=ROUND_DOWN)
        return None

    def __sold_tokens(self) -> None:
        for k in self._tokens.keys():
            contract = self._tokens[k]
            balance = self['tokens'][k]['balance']
            token_price_in_ether = Decimal(contract.token_price())  # Assuming you have this attribute
            ether_price = Infos.ether_price

            for kp, vp in ether_price.items():
                decimal_value = Decimal(vp)
                self['tokens'][k]['sold'][kp] = (balance * token_price_in_ether * decimal_value).quantize(ROUNDING_PRECISION, rounding=ROUND_DOWN)

    def __sold_total(self) -> None:
        sold_eth = self['eth']['sold']
        total = {k: v for k, v in sold_eth.items()}
        
        for k in self._tokens.keys():
            sold_token = self['tokens'][k]['sold']
            for ksold, vsold in sold_token.items():
                if ksold not in total:
                    total[ksold] = Decimal('0')
                total[ksold] = (total[ksold] + vsold).quantize(ROUNDING_PRECISION, rounding=ROUND_DOWN)

        self['total'] = total

    def update_total(self) -> None:
        self.__sold_total()

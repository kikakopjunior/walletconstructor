from walletconstructor.wallet import get_wallet
from walletconstructor.security.config import ConfigEnv


def set_path_dirname(path:str) -> None:
    ConfigEnv.set_path_dirname(path)